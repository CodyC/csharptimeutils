﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeUtilities
{
    public class TimeUtils
    {
        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            
            return dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        }


        public static Double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
        }

        public static DateTime? UnixTimeStampToDateTime(Double? unixTimeStamp)
        {
            System.DateTime? dtDateTime = null;

            if (unixTimeStamp.HasValue)
            {
                dtDateTime = UnixTimeStampToDateTime(unixTimeStamp.Value);
            }

            return dtDateTime;
        }
        
        public static Double? DateTimeToUnixTimestamp(DateTime? dateTime)
        {
            Double? unixTimeStamp = null;

            if (dateTime.HasValue) 
            {
                unixTimeStamp = DateTimeToUnixTimestamp(dateTime.Value);
            }

            return unixTimeStamp;
        }
    }
}
